# react-wordpress-auth

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/react-wordpress-auth.svg)](https://www.npmjs.com/package/react-wordpress-auth) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-wordpress-auth
```

Wrap your `App` with the `AuthProvider` in `App.js`

```jsx
import React from 'react'
import { AuthProvider } from 'react-wordpress-auth'
const App = () => {
  return <AuthProvider>{/* ... */}</AuthProvider>
}
```

## Usage

```jsx
import React from 'react'
import { useForm } from 'react-hook-form'
import { useAuth } from 'react-wordpress-auth'

const Login = () => {
  const { handleSubmit, register, errors } = useForm()
  const {
    user,
    loading,
    isAuthenticated,
    login,
    signup,
    isAuthenticating
  } = useAuth()

  if (isAuthenticated) {
    return <div>Welcome {user.firstName}!</div>
  }

  return (
    <form onSubmit={handleSubmit(login)}>
      <label for='email'>Email</label>
      <input type='email' name='email' ref={register} />
      {errors.email && errors.email.message}

      <label for='password'>Password</label>
      <input type='password' name='password' ref={register} />
      {errors.password && errors.password.message}

      <button type='submit' disabled={loading}>
        {loading ? 'Loading...' : 'Login'}
      </button>
    </form>
  )
}
```

## License

MIT © [Moretape](https://github.com/moretape)
