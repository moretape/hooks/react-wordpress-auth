<?php

require_once('vendor/autoload.php');

//
// Load ACF Components
//
// require_once('components/index.php');

//
// Load API endpoints
//
// require_once('restful.php');
require_once('restful/index.php');

//
// Load React dev enviroment
//
require_once('react.php');

add_action( 'wp_head', 'my_header_scripts' );
function my_header_scripts(){
  ?>
  <script>window.moretape = { unlocked: true }</script>
  <?php
}

// function authorized($authorized, $order_id = null, $input) {
// 	if (!$authorized) {
// 		$user = wp_get_current_user();
//
// 		if (!empty($user)) {
// 			$user_id = $user->data->ID;
// 			$customer_id = $input['customerId'];
//
// 			if (!isset($customer_id) || $customer_id === $user_id) {
// 				return true;
// 			}
// 		}
// 	}
//
// 	return $authorized;
// }
// add_filter('graphql_woocommerce_authorized_to_create_orders', 'authorized', 1, 3);
//
// function assign_customer_id_to_current_user($order){
// 	$user = wp_get_current_user();
//
// 	if (!empty($user)) {
// 		$user_id = $user->data->ID;
// 		$customer_id = $order->get_customer_id();
//
// 		if (!$customer_id) {
// 			$order->set_customer_id($user_id);
// 			$order->save();
// 		}
// 	}
//
// 	return $order;
// }
// add_filter('graphql_woocommerce_after_order_create', 'assign_customer_id_to_current_user');

//
// Wordpress related
//
//
add_filter('show_admin_bar', function($show) { return false; });

function bbr_deregister_scripts_and_styles(){
    wp_deregister_style('storefront-woocommerce-style');
    wp_deregister_style('storefront-style');
    wp_deregister_style('storefront-icons');
}
add_action('wp_print_styles', 'bbr_deregister_scripts_and_styles', 100);

function bbr_remove_storefront_standard_functionality() {
  // remove customizer inline styles from parent theme as I don't need it.
  set_theme_mod('storefront_styles', '');
  set_theme_mod('storefront_woocommerce_styles', '');
}
add_action('init', 'bbr_remove_storefront_standard_functionality');

function blog_favicon() { ?>
	<link rel="shortcut icon" href="<?php echo bloginfo('stylesheet_directory') ?>/favicon.ico" >
<?php }
add_action('wp_head', 'blog_favicon');

function addPriceSuffix($format, $currency_pos) {
	switch ( $currency_pos ) {
		case 'left' :
			$currency = get_woocommerce_currency();
			$format = '%1$s%2$s ' . $currency;
		break;
	}

	return $format;
}

add_action('woocommerce_price_format', 'addPriceSuffix', 1, 2);
