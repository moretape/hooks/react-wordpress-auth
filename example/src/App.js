import React from 'react'
import { ApolloProvider } from '@apollo/react-hooks'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { AuthProvider } from 'react-wordpress-auth'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// add graphql middleware
import { middleware } from 'react-wordpress-auth'

import Login from './auth/Login'
import Register from './auth/Register'
import ForgotPassword from './auth/FotgotPassword'
import ResetPassword from './auth/ResetPassword'

const cache = new InMemoryCache()
const httpLink = new HttpLink({
  uri: '/graphql'
})

const client = new ApolloClient({
  link: middleware.concat(httpLink),
  cache
})

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <AuthProvider>
          <Switch>
            <Route
              path="/login"
              component={Login}
              exact
            />
            <Route
              path="/register"
              component={Register}
              exact
            />
            <Route
              path="/forgot-password"
              component={ForgotPassword}
              exact
            />
            <Route
              path="/reset-password"
              component={ResetPassword}
              exact
            />
          </Switch>
        </AuthProvider>
      </Router>
    </ApolloProvider>
)
}

export default App
