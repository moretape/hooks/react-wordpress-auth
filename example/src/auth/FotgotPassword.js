import React from 'react'
import { useForm } from 'react-hook-form'
import { useAuth } from '../../../'

const ForgotPassword = () => {
  const { handleSubmit, register, errors } = useForm()
  const { loading, forgotPassword } = useAuth()

  return (
    <>
      <form onSubmit={handleSubmit(forgotPassword)}>
        <p>email to send new password</p>
        <input type='email' name='email' ref={register} />
        {errors.email && errors.email.message}

        <button type='submit' disabled={loading}>
          {loading ? 'Loading...' : 'Send'}
        </button>
      </form>
    </>
  )
}

export default ForgotPassword
