import React from 'react'
import { useForm } from 'react-hook-form'
import { useAuth } from '../../../'

const Login = () => {
  const { handleSubmit, register, errors } = useForm()
  const { user, loading, isAuthenticated, login, logout } = useAuth()

  if (isAuthenticated) {
    return (
      <div>
        Welcome {user.firstName}
        <button onClick={logout}>log out</button>
      </div>
    )
  }

  return (
    <>
      <form onSubmit={handleSubmit(login)}>
        <p>email</p>
        <input name='email' ref={register} />
        {errors.email && errors.email.message}

        <p>password</p>
        <input type='password' name='password' ref={register} />
        {errors.password && errors.password.message}

        <button type='submit' disabled={loading}>
          {loading ? 'Loading...' : 'Login'}
        </button>
      </form>
    </>
  )
}

export default Login
