import React from 'react'
import { useForm } from 'react-hook-form'
import { useAuth } from '../../../'

const Login = () => {
  const { handleSubmit, register, errors } = useForm()
  const { user, loading, isAuthenticated, signup, logout } = useAuth()

  if (isAuthenticated) {
    return (
      <div>
        Welcome {user.firstName}
        <button onClick={logout}>log out</button>
      </div>
    )
  }

  return (
    <>
      <form onSubmit={handleSubmit(signup)}>
        <p>email</p>
        <input
          name='email'
          ref={register}
        />
        {errors.email && errors.email.message}

        <p>first name</p>
        <input
          name='firstName'
          ref={register}
        />
        {errors.firstName && errors.firstName.message}

        <p>last name</p>
        <input
          name='lastName'
          ref={register}
        />
        {errors.lastName && errors.lastName.message}

        <p>password</p>
        <input
          name='password'
          ref={register}
        />
        {errors.password && errors.password.message}

        <button type='submit' disabled={loading}>
          {loading ? 'Loading...' : 'Signup'}
        </button>
      </form>
    </>
  )
}

export default Login
