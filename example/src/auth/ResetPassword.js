import React from 'react'
import { useForm } from 'react-hook-form'
import { useAuth } from '../../../'

const Login = () => {
  const { handleSubmit, register, errors } = useForm()
  const { user, loading, isAuthenticated, resetPassword, logout } = useAuth()

  if (isAuthenticated) {
    return (
      <div>
        Welcome {user.firstName}
        <button onClick={logout}>log out</button>
      </div>
    )
  }

  return (
    <>
      <form onSubmit={handleSubmit(resetPassword)}>
        <p>new password</p>
        <input type='password' name='password' ref={register} />
        {errors.password && errors.password.message}

        <button type='submit' disabled={loading}>
          {loading ? 'Loading...' : 'Reset'}
        </button>
      </form>
    </>
  )
}

export default Login
