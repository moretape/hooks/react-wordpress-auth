export * from "./FotgotPassword";
export { default } from "./FotgotPassword";

export * from "./Login";
export { default } from "./Login";


export * from "./ResetPassword";
export { default } from "./ResetPassword";


export * from "./Register";
export { default } from "./Register";


