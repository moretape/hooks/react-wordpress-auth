const proxy = require('http-proxy-middleware')

module.exports = function (app) {
  const wordpress = proxy({
    target: 'http://localhost',
    changeOrigin: true
  })

  app.use('/graphql', wordpress)
  app.use('/wp-json', wordpress)
  app.use('/wp-admin', wordpress)
  app.use('/wp-content', wordpress)
  app.use('/wp-includes', wordpress)
  app.use('/wp-login.php', wordpress)
}
