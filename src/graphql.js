import React, { useState, useCallback, useContext, createContext } from 'react'
import { ApolloLink } from 'apollo-link'
import { useMutation, useQuery } from '@apollo/react-hooks'
import qs from 'query-string'
import gql from 'graphql-tag'
import useInterval from 'use-interval'

const LOGIN = gql`
  mutation login($username: String!, $password: String!) {
    login(
      input: { clientMutationId: "", username: $username, password: $password }
    ) {
      authToken
      user {
        userId
        firstName
        lastName
        email
        jwtUserSecret
        jwtRefreshToken
        jwtAuthToken
        jwtAuthExpiration
      }
    }
  }
`

const SIGNUP = gql`
  mutation signup(
    $username: String!
    $email: String!
    $password: String!
    $firstName: String!
    $lastName: String!
  ) {
    registerUser(
      input: {
        clientMutationId: ""
        username: $username
        firstName: $firstName
        lastName: $lastName
        email: $email
        password: $password
      }
    ) {
      user {
        userId
        firstName
        lastName
        email
        jwtUserSecret
        jwtRefreshToken
        jwtAuthToken
        jwtAuthExpiration
      }
    }
  }
`

const VERIFYTOKEN = gql`
  query verifyToken {
    viewer {
      userId
      firstName
      lastName
      email
      jwtUserSecret
      jwtRefreshToken
      jwtAuthToken
      jwtAuthExpiration
    }
  }
`

const FORGOTPASSWORD = gql`
  mutation sendPasswordEmail($username: String!) {
    sendPasswordResetEmail(
      input: { clientMutationId: "", username: $username }
    ) {
      user {
        email
      }
    }
  }
`

const RESETPASSWORD = gql`
  mutation resetPassword($key: String!, $login: String!, $password: String!) {
    resetUserPassword(
      input: {
        clientMutationId: ""
        key: $key
        login: $login
        password: $password
      }
    ) {
      user {
        userId
        firstName
        lastName
        email
      }
    }
  }
`

const TOKEN_EXPIRY_SECONDS = 300
const AuthContext = createContext()
const localStorage = window.localStorage
const getToken = () => {
  try {
    return JSON.parse(localStorage.getItem('token'))
  } catch (e) {
    localStorage.removeItem('token')
  }
  return null
}
const setToken = (token) => localStorage.setItem('token', JSON.stringify(token))

export const AuthProvider = ({ children }) => {
  const auth = useProvideAuth()
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>
}

export const useAuth = (options) => useContext(AuthContext)

/**
 * Middleware operation
 * If we have a session token in localStorage, add it to the GraphQL request as a Session header.
 */
export const middleware = new ApolloLink((operation, forward) => {
  const headers = {}

  const token = getToken()
  if (token) headers.Authorization = `Bearer ${token}`

  operation.setContext({ headers })
  return forward(operation)
})

/**
 * Hook useAuth
 */
export const useProvideAuth = (options) => {
  const { history } = options || {}
  const [loginMutation, { loading: loginLoading }] = useMutation(LOGIN)
  const [signupMutation, { loading: signupLoading }] = useMutation(SIGNUP)
  const [sendPasswordMutation, { loading: sendingPassword }] = useMutation(
    FORGOTPASSWORD
  )
  const [resetPasswordMutation, { loading: resetingPassword }] = useMutation(
    RESETPASSWORD
  )
  const { refetch: verifyToken, loading: verifyingToken } = useQuery(
    VERIFYTOKEN
  )
  const [user, setUser] = useState(null)
  const isAuthenticated = user != null
  const token = getToken()

  const redirect = (options) => {
    if (history) {
      const { defaultRedirect } = options || {}
      const { pathname } = window.location
      const { redirect } = qs.parse(window.location.search)
      const currentPath = `${pathname || '/'}`
      const redirectTo =
        redirect && redirect !== currentPath ? redirect : defaultRedirect
      if (redirectTo) history.push(redirectTo)
    }
  }

  const login = async (variables, options) => {
    if (!variables.username) variables.username = variables.email

    const { data } = await loginMutation({ variables })
    if (data && data.login) setUser(data.login.user)

    setToken(data.login.user.jwtAuthToken)
    redirect(options)
    return data.login && data.login.user
  }

  const signup = async (variables, options) => {
    if (!variables.username) variables.username = variables.email

    const { data } = await signupMutation({ variables })
    if (data && data.registerUser) setUser(data.registerUser.user)

    setToken(data.registerUser.user.jwtAuthToken)
    redirect(options)
    return data.registerUser && data.registerUser.user
  }

  const logout = useCallback(() => {
    setUser(null)
    localStorage.removeItem('token')
  }, [user])

  const forgotPassword = async (variables) => {
    // only need username || email
    if (!variables.username) variables.username = variables.email

    const { data } = await sendPasswordMutation({ variables })
    return data && data.sendPasswordResetEmail
  }

  const resetPassword = async (variables) => {
    // need key (token) && login (username or email) && new password
    const { key, login } = qs.parse(window.location.search)
    const { data } = await resetPasswordMutation({
      variables: { key, login, password: variables.password }
    })
    return data && data.resetUserPassword
  }

  useInterval(
    () => {
      const init = async () => {
        try {
          const { data } = await verifyToken()
          const user = data && data.viewer
          if (user) {
            setUser(user)
            setToken(user.jwtAuthToken)
          } else {
            logout()
          }
        } catch (e) {
          console.log(e)
          logout()
        }
      }
      init()
    },
    token && TOKEN_EXPIRY_SECONDS * 1000,
    token !== null
  )

  return {
    authenticating: token && !isAuthenticated,
    loading:
      loginLoading ||
      signupLoading ||
      verifyingToken ||
      sendingPassword ||
      resetingPassword,
    login,
    isAuthenticated,
    user,
    signup,
    logout,
    forgotPassword,
    resetPassword
  }
}

export default useAuth
